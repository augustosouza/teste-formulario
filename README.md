# Formulário com Go e Python

![](imagens/docker-python-go.jpg)

#### Tecnologias envolvidas

|   Produto - Nome    |  Versão - Release  | Fornecedor         |
|:-------------------:|:------------------:|:------------------:|
| docker              |  18.3 ou superior  | Docker             |
| docker-compose      |  1.25.3            | Docker             |
| Python              |  3.7 ou superior   | Python.org         |
| Golang              |  1.14              | Golang.org         |

Para melhor aproveitamento dos recursos, foi decidido utilizar imagens oficiais para realizar o build.

#### Clonando o Projeto via SSH

* Em **Clone** copie o link do projeto

* Abrir o Terminal Linux ou MacOS ou Git Bash no Windows e colar o link gerado 


```
$ git clone git@gitlab.com:augustosouza/teste-formulario.git

```

* Criar uma nova Branch para desenvolvimento do codigo

```
$ git branch <branch_name>

$ git checkout <branch_name>

```

#### Build da aplicação Simplificada

* Para simplificar a utilização, será usado o **docker-compose** onde está agrupado todas as instruções necessárias para subir o ambiente.
* Na Raiz do projeto, usar o comando descrito abaixo:

```
$ docker-compose up -d

```
* A saida deverá ser semelhante a seguinte:

```
$ Creating teste-formulario_backend_1 ... done
$ Creating teste-formulario_frontend_1 ... done

```
#### Validar se Serviços estão no Ar

* Para validar se o serviços estão no ar após build, no browser digitar o seguinte URL: http://localhost:8000/healthz
* A saida deverá ser:

``` html
{
  "backend_status": "ok", 
  "frontend_status": "ok"
}

```
Possivel tambem utilizar o comando cURL (curl http:/localhost:8000/healthz) a saida será a mesma

#### Validar\Modificar serviços individualmente

* Dentro de cada estrutura dos serviços haverá um **Dockerfile** criado, e este Dockerfile é responsavel por criar a imagem de build do projeto.
* Para dar build no serviço de forma individual, você deverá acessar o diretorio do projeto e no primeiro nivel, o Dockerfile estará disponivel.

```
.
├── backend
│   ├── **Dockerfile**
│   └── src
│       └── backend
│           ├── main.go
│           └── README.md
├── docker-compose.yml
├── frontend
│   ├── **Dockerfile**
│   └── src
│       └── frontend
│           ├── frontend.py
│           ├── README.md
│           ├── requirements.txt
│           ├── static
│           │   ├── favicon.ico
│           │   └── style.css
│           └── templates
│               └── index.html
└── README.md

```

* No exemplo abaixo, segue como dar build no Frontend:

```
$ cd frontend
$
$ docker build -t <tag_name> .

```
-t: Tag do imagem

* A sáida deverá ser a seguinte:

```
Successfully built 04a085b5aca2
Successfully tagged <tag_name>

```

* Para subir a aplicação, utilizar o seguinte comando:

```
$ docker run -it -d -p 8000:8000 <tag_name>

```

-it: terminal interativo
-d: Para serviço rodar em background
-p: porta (host:container)

Para não rodar em background, substituir o -d por --rm


* Para validar se o serviço está no ar

```
$ curl http://localhost:8000/healthz

```
* Para acessar o serviço via browser:

http://localhost:8000


##### Links

- https://www.docker.com/
- https://golang.org/
- https://python.org/

#### FAQ - CONTATO 

##### Maintainer: Augusto Souza

##### E-mail: augusto_souza@outlook.com.br